# INF560
**Requirements:**
- Python 3.8.2



**Installations:**
- pip install django
- pip install pandas
- pip install django-estimators
- pip3 install sklearn
- pip install xgboost-1.0.2-cp38-cp38-win32.whl
- pip install matplotlib
- pip install mpld3
- pip install jinja2
- pip3 install plotly
- pip install djangorestframework


<br>
<br>

**Dashboard**
**Command to run:**
 - Go to the folder/directory in command prompt ->
 - py manage.py runserver ->
 - To access the website in browser use link: http://127.0.0.1:8000/
