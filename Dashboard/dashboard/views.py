from django.shortcuts import get_object_or_404, render
from django.contrib import auth
from django.contrib import messages
import pandas as pd
from sklearn.metrics.pairwise import cosine_similarity
from sklearn.feature_extraction.text import TfidfVectorizer
import json
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core import serializers
import math
import xgboost as xgb
from sklearn.model_selection import GridSearchCV
import numpy as np
import sklearn
from sklearn import metrics
import pickle
import matplotlib
import matplotlib.pyplot as plt, mpld3
import plotly.express as px
from datetime import date, datetime, time, timedelta

def welcome(request):
    context = {"welcome_page": "active"}
    return render(request,"welcome.html")

def admin_login(request):
    context = {"admin_login": "active"}
    return render(request,"admin_login.html")

def postlogin(request):
    userid = request.POST.get("userid")
    password = request.POST.get("password")
    if (userid=="admin" and password=="rioadm"):
        return render(request,"adm_home.html",{'loggeduser':userid})
    else:
        messages.warning(request, messages.INFO, 'Invalid Credentials! Please try again.')
        return render(request,"admin_login.html")
    return render(request,"admin_login.html",{'loggeduser':userid})

def adm_home(request):
    context = {"adm_home": "active"}
    return render(request,"adm_home.html")

def flight_details(request):
    context = {"flight_details": "active"}
    return render(request,"flight_number.html")

def passenger_home(request):
    context = {"passenger_home": "active"}
    return render(request,"passenger_home.html")

def airport_spa(request):
    context = {"airport_spa": "active"}
    return render(request, "airport_spa.html")

def airport_lounge(request):
    context = {"airport_lounge": "active"}
    return render(request, "airport_lounge.html")

def airport_coffee(request):
    context = {"airport_coffee": "active"}
    return render(request, "airport_coffee.html")

def airport_resta(request):
    context = {"airport_resta": "active"}
    return render(request, "airport_resta.html")

def airport_fast(request):
    context = {"airport_fast": "active"}
    return render(request, "airport_fast.html")

def airport_food(request):
    context = {"airport_food": "active"}
    return render(request, "airport_food_all.html")

def airport_sight(request):
    context = {"airport_sight": "active"}
    return render(request, "airport_sightseeing.html")

def airport_salon(request):
    context = {"airport_salon": "active"}
    return render(request, "airport_salon.html")

def airport_shopping(request):
    context = {"airport_shopping": "active"}
    return render(request, "airport_shopping.html")

def airport_beauty(request):
    context = {"airport_beauty": "active"}
    return render(request, "airport_beauty.html")

def airport_books(request):
    context = {"airport_books": "active"}
    return render(request, "airport_books.html")

def airport_malls(request):
    context = {"airport_malls": "active"}
    return render(request, "airport_malls.html")

def airport_pharmacy(request):
    context = {"airport_pharmacy": "active"}
    return render(request, "airport_pharmacy.html")

def airport_fashion(request):
    context = {"airport_fashion": "active"}
    return render(request, "airport_fashion.html")

def rio_hotels(request):
    context = {"rio_hotels": "active"}
    return render(request, "rio_hotels_all.html")

def rio_lodge(request):
    context = {"rio_lodge": "active"}
    return render(request, "rio_lodge.html")

def rio_hotel_filter(request):
    context = {"rio_hotel_filter": "active"}
    return render(request, "rio_hotels.html")

def rio_hotel_bnb(request):
    context = {"rio_hotel_bnb": "active"}
    return render(request, "rio_bnb.html")

def rio_hotel_info(request):
    context = {"rio_hotel_info": "active"}
    hotel_id = request.GET.get('hotelID')
    df = pd.read_json('static/data/Rio_Hotels_Clean.json', orient="index")
    hotel_name = df.loc[int(hotel_id)]['HotelName']
    df.set_index('HotelName', inplace = True)
    tf = TfidfVectorizer(analyzer='word', ngram_range=(1, 3), min_df=0, stop_words='english')
    tfidf_matrix = tf.fit_transform(df['Description_Clean'])
    cosine_similarities = cosine_similarity(tfidf_matrix, tfidf_matrix)
    indices = pd.Series(df.index)
    recommended_hotels = []
    idx = indices[indices == hotel_name].index[0]
    score_series = pd.Series(cosine_similarities[idx]).sort_values(ascending = False)
    top_10_indexes = list(score_series.iloc[1:11].index)
    for i in top_10_indexes:
        recommended_hotels.append(list(df.index)[i])
    return render(request, "rio_hotel_info.html", {'recommendations': json.dumps(recommended_hotels)})

def rio_dining(request):
    context = {"rio_dining": "active"}
    return render(request, "rio_dine_all.html")

def rio_bakery(request):
    context = {"rio_bakery": "active"}
    return render(request, "rio_bakery.html")

def rio_coffee(request):
    context = {"rio_coffee": "active"}
    return render(request, "rio_coffee.html")

def rio_dessert(request):
    context = {"rio_dessert": "active"}
    return render(request, "rio_dessert.html")

def rio_restaurants(request):
    context = {"rio_restaurants": "active"}
    return render(request, "rio_resta.html")

def rio_dine_info(request):
    context = {"rio_dine_info": "active"}
    id = request.GET.get('restId')
    df = pd.read_json('static/data/restaurants.json', orient="index")
    name = df.loc[int(id)]['Name']
    df.set_index('Name', inplace = True)
    tf = TfidfVectorizer(analyzer='word', ngram_range=(1, 3), min_df=0, stop_words='english')
    tfidf_matrix = tf.fit_transform(df['Description_Clean'])
    cosine_similarities = cosine_similarity(tfidf_matrix, tfidf_matrix)
    indices = pd.Series(df.index)
    recommended_restaurants = []
    idx = indices[indices == name].index[0]
    score_series = pd.Series(cosine_similarities[idx]).sort_values(ascending = False)
    top_10_indexes = list(score_series.iloc[1:11].index)
    for i in top_10_indexes:
        recommended_restaurants.append(list(df.index)[i])
    return render(request, "rio_dine_info.html", {'recommendations': json.dumps(recommended_restaurants)})

def adm_predict(request):
    context = {"adm_predict": "active"}
    filename = 'static/data/XGBoost_passengers.sav'
    date = request.POST.get('date')
    date_arr = date.split('/')
    building = request.POST.get('building')
    level = request.POST.get('floor')
    xgboost_model = pickle.load(open(filename, 'rb'))
    model_input = [int(date_arr[1]), int(date_arr[0]), 0, int(building), int(level)]
    x = []
    y = []
    for hour in range(24):
      x.append(hour)
      model_input[2] = hour
      model_df = pd.DataFrame([model_input],columns = ["day","month","hour","Building","Level"])
      y_pred_model = xgboost_model.best_estimator_.predict(model_df)
      y.append(abs(y_pred_model[0]))
    df = pd.DataFrame()
    df["Hours"] = x
    df["Number of passengers"] = y
    df["Number of passengers"] = df["Number of passengers"].round()
    fig2 = px.line(df, x="Hours", y="Number of passengers", title='Predictions for the date: ' +date)
    graph = fig2.to_html(full_html=False, default_height=500, default_width=800)
    return render(request, "adm_predict.html",{'div_figure' : graph})

def adm_crowd(request):
    context = {"adm_crowd": "active"}
    return render(request, "adm_predict.html")


def adm_staff(request):
    context = {"adm_staff": "active"}
    return render(request, "adm_staff.html")

origins_languages={'Buenos Aires':['Spanish'], 'Fortaleza':['Portuguese'], 'Luxembourg':['Luxembourgish','French', 'German'], 'Miami':['English', 'Spanish'], 'Panama City':['Spanish'], 'Paris':['French'], 'Santa Cruz':['Spanish'], 'Santiago':['Spanish'], 'Sao Paulo':['Portuguese']}

def assign_ethnicity(origin):
  city = str(origin)
  ethnicities = origins_languages[city]
  return ','.join(ethnicities)

def calculate_time(schedule, arrival):
  if pd.isnull(arrival):
    return schedule
  else:
    return arrival

def adm_staff_pred(request):
    context = {"adm_staff_pred": "active"}
    hour = request.POST.get('hour')
    date = request.POST.get('date')
    date_arr = date.split('/')
    building = request.POST.get('building')
    level = request.POST.get('floor')
    filename_p = 'static/data/XGBoost_passengers.sav'
    model_p = pickle.load(open(filename_p, 'rb'))
    filename_e = 'static/data/XGBoost_employees.sav'
    model_e = pickle.load(open(filename_e, 'rb'))
    filename_h = 'static/data/XGBoost_hours.sav'
    model_h = pickle.load(open(filename_h, 'rb'))
    model_input = [int(date_arr[1]), int(date_arr[0]), 0, int(building), int(level)]
    x = []
    #passengers
    y_p = []
    #employees
    y_e = []
    #mean_wait_hours
    y_h = []
    passenger = []
    c_employee = []
    for hours in range(24):
      x.append(hours)
      model_input[2] = hours
      model_df = pd.DataFrame([model_input],columns = ["day","month","hour","Building","Level"])
      y_pred_model_p = model_p.predict(model_df)
      y_pred_model_e = model_e.predict(model_df)
      y_pred_model_h = model_h.predict(model_df)
      y_p.append(math.ceil(abs(y_pred_model_p[0])))
      y_e.append(math.ceil(abs(y_pred_model_e[0])))
      y_h.append(abs(y_pred_model_h[0]))
      passenger.append("Passenger")
      c_employee.append("Current Employee")
    s_employee = []
    s_passenger = []
    suggested = []
    historical = []
    for hours_p in range(24):
      curr_p = y_p[hours_p]
      curr_h = y_h[hours_p]
      # formula
      a = 3
      b = -0.9
      ratio = a*curr_h**b
      suggested_e = curr_p/ratio
      suggested.append(suggested_e)
      s_employee.append("Suggested Employee")
      s_passenger.append("Suggested Wait Time")
      historical.append("Historical Wait Time")
    df1 = pd.DataFrame()
    df1['Category'] = passenger
    df1['Predicted numbers'] = y_p
    df1['Hours'] = x
    df2 = pd.DataFrame()
    df2['Category'] = c_employee
    df2['Predicted numbers'] = y_e
    df2['Hours'] = x
    df3 = pd.DataFrame()
    df3['Category'] = s_employee
    df3['Predicted numbers'] = suggested
    df3['Predicted numbers'] = df3['Predicted numbers'].apply(np.ceil)
    df3['Hours'] = x
    df1 = df1.append(df2, ignore_index = True)
    df1 = df1.append(df3, ignore_index = True)
    fig1 = px.line(df1, x="Hours", y="Predicted numbers", color='Category', title='Predictions for the date: ' +date)
    graph1 = fig1.to_html(full_html=False, default_height=500, default_width=800)
    temp = pd.DataFrame()
    temp['passengers'] = y_p
    temp['suggested_employees'] = suggested
    temp['historical_employees'] = y_e
    temp['historical_ratio'] = temp['passengers']/temp['historical_employees']
    temp['ratio'] = temp['passengers']/temp['suggested_employees']
    temp['mean_wait_hours']=y_h
    temp['suggested_employees'] = temp['suggested_employees'].apply(lambda x: math.ceil(x))
    temp['hour'] = temp.index
    df4 = pd.DataFrame()
    df4['Mean Wait Hours'] = temp['mean_wait_hours']
    df4['Ratio'] = temp['ratio']
    df4['Category'] = s_passenger
    df5 = pd.DataFrame()
    df5['Mean Wait Hours'] = temp['mean_wait_hours']
    df5['Ratio'] = temp['historical_ratio']
    df5['Category'] = historical
    df4 = df4.append(df5, ignore_index = True)
    #fig2 = px.line(df4, x="Mean Wait Hours", y="Ratio", color='Category', title='Relation between ratio & mean wait hours')
    #graph2 = fig2.to_html(full_html=False, default_height=500, default_width=800)
    #flights sechdule
    flights = pd.read_csv('static/data/flights.csv')
    flights['Date'] = flights['Date'].apply(pd.to_datetime)
    flights['Scheduled'] = flights['Scheduled'].apply(pd.to_datetime).dt.time
    flights['Arrival'] = flights['Arrival'].apply(pd.to_datetime).dt.time
    flights['From'] = flights['From'].apply(lambda x: str(x).replace(u'\xa0', u' ')[:-4])
    flights['Arrival'] = flights.apply(lambda x: calculate_time(x.Scheduled, x.Arrival), axis=1)
    flights['Ethnicities'] = flights['From'].apply(lambda x: assign_ethnicity(x))
    flights['Hour'] = flights['Arrival'].apply(lambda x: x.hour)
    flights['Weekday'] = flights['Date'].apply(lambda x: x.weekday())
    hour = int(hour)
    day = '0' + str(model_input[0]) if len(str(model_input[0])) == 1 else str(model_input[0])
    month = '0' + str(model_input[1]) if len(str(model_input[1])) == 1 else str(model_input[1])
    date = datetime.strptime('2020' + '-' + str(month) + '-' + str(day), '%Y-%m-%d')
    weekday = date.weekday()
    ethnicities = flights[(flights['Weekday']==weekday) & (flights['Hour']==hour)][['Hour', 'Ethnicities']]
    employees = temp[temp['hour']==hour]
    all_ethnicities = []
    default_languages = ['English', 'Portuguese', 'Spanish']

    if not ethnicities.empty:
      #join ethnicitis with employees
      ethnicity_suggestion = employees.merge(ethnicities, how='inner', left_on='hour', right_on='Hour')
      employees_ethnicties = list(ethnicity_suggestion['Ethnicities'].values)
      #ethnicities of arrvial flights
      for item in employees_ethnicties:
        ethnicities_list = item.split(',')
        all_ethnicities.extend(ethnicities_list)

    # print(all_ethnicities)

    #number of suggested emoloyees
    suggested_employees = employees['suggested_employees'].values[0]

    #combination of employees ethnicities
    employees_combination = {}
    if not all_ethnicities:
      for i in range(len(default_languages)):
        e = default_languages[i]
        if i != len(default_languages)-1:
          employees_combination[e] = int(round(suggested_employees/len(default_languages)))
          # print(employees_combination)
        else:
          employees_combination[e] = suggested_employees-sum(employees_combination.values())
          # print(employees_combination)
    else:
      emp_70 = int(round(suggested_employees*0.7))
      emp_30 = int(round(suggested_employees - emp_70))
      for i in range(len(all_ethnicities)):
        e = all_ethnicities[i]
        if i != len(all_ethnicities)-1:
          employees_combination[e] = int(round(emp_70/len(all_ethnicities)))
        else:
          employees_combination[e] = emp_70-sum(employees_combination.values())

      for l in default_languages:
        if l in all_ethnicities:
          default_languages.remove(l)

      for i in range(len(default_languages)):
        e =default_languages[i]
        if i != len(default_languages)-1:
          employees_combination[e] = int(round(emp_30/len(default_languages)))
        else:
          employees_combination[e] = suggested_employees-sum(employees_combination.values())
    df6 = pd.DataFrame()
    df6['Language'] = employees_combination.keys()
    df6['Ratio'] = employees_combination.values()
    fig3 = px.pie(df6, values='Ratio', names='Language')
    graph3 = fig3.to_html(full_html=False, default_height=500, default_width=800)
    return render(request, "adm_staff.html",{'div_figure1' : graph1, 'div_figure3' : graph3, 'hour': hour})
